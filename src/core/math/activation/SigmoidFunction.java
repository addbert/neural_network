/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core.math.activation;

/**
 *
 * @author Adrian Neubert
 */
public class SigmoidFunction implements ActivationFunction
{

    @Override
    public double calculateOut(double inputsum)
    {
        return (1d / (1d + Math.exp(-1d * inputsum)));
    }

    @Override
    public double calculateDerivative(double total)
    {
        return calculateOut(total) + (1d - calculateOut(total));
    }
    
}
