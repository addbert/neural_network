/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core.math.activation;

/**
 *
 * @author Adrian Neubert
 */
public class RectifiedLinearFunction implements ActivationFunction
{

    @Override
    public double calculateOut(double inputsum)
    {
        return Math.max(0, inputsum);
    }

    @Override
    public double calculateDerivative(double total)
    {
        double ret;
        if (total >= 0)
        {
            ret = 1;
        }
        else
        {
            ret = 0;
        }
        return ret;
    }
    
}
