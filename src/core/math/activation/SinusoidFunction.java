/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core.math.activation;

/**
 *
 * @author Adrian Neubert
 */
public class SinusoidFunction implements ActivationFunction
{

    @Override
    public double calculateOut(double inputsum)
    {
        return Math.sin(inputsum);
    }

    @Override
    public double calculateDerivative(double total)
    {
        return Math.cos(total);
    }
    
}
