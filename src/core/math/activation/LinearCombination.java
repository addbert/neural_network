/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core.math.activation;

/**
 *
 * @author Adrian Neubert
 */
public class LinearCombination implements ActivationFunction
{
    
    private double bias;

    @Override
    public double calculateOut(double inputsum)
    {
        return inputsum + bias;
    }

    @Override
    public double calculateDerivative(double total)
    {
        return 0;
    }
    
}
