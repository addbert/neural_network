package core.math.sum;

import java.util.List;

import core.netmember.NConnection;
/**
 *
 * @author Adrian Neubert
 */
public interface InputFunction 
{
    double collectOut(List<NConnection> inputConnections);
}
