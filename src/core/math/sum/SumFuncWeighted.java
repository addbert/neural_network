package core.math.sum;

import java.util.List;

import core.netmember.NConnection;

public class SumFuncWeighted implements InputFunction
{

	@Override
	public double collectOut(List<NConnection> inputConnections) 
	{
        double weightedSum = 0d;
        for (NConnection con : inputConnections)
        {
            weightedSum += con.getWeightedIn();
        }
        
        return weightedSum;
	}

}
