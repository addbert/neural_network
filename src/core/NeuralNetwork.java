package core;

import java.util.List;

import core.netmember.NLayer;

public class NeuralNetwork 
{
	private String id;
	private NLayer inputLayer, outputLayer;
	private List<NLayer> hiddenlayer;
	
	public NeuralNetwork(String id, NLayer inputLayer, List<NLayer> hiddenlayer, NLayer outputLayer)
	{
		this.id = id;
		this.inputLayer = inputLayer;
		this.hiddenlayer = hiddenlayer;
		this.outputLayer = outputLayer;
	}

	public String getId() 
	{
		return id;
	}

	public NLayer getInputLayer() 
	{
		return inputLayer;
	}

	public NLayer getOutputLayer() 
	{
		return outputLayer;
	}

	public List<NLayer> getHiddenlayer() 
	{
		return hiddenlayer;
	}
	
	
}
