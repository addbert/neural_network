/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core.netmember;

import core.math.activation.ActivationFunction;
import core.math.sum.SumFuncWeighted;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Adtrian Neubert
 */
public class Neuron
{
    private String id;
    private List<NConnection> inputCons;
    private List<NConnection> outputCons;
    private SumFuncWeighted sumFunc;
    private ActivationFunction actFunc;
    
    public Neuron()
    {
        this.inputCons = new ArrayList<>();
        this.outputCons = new ArrayList<>();
    }
    
    public Neuron(SumFuncWeighted inputFunc, ActivationFunction activFunc)
    {
        this();
        this.actFunc = activFunc;
        this.sumFunc = inputFunc;
    }
    
    
    public double calculateoutput()
    {
        double total = sumFunc.collectOut(inputCons);
        return actFunc.calculateOut(total);
    }
    
    public boolean hasInputCon()
    {
        boolean ret = false;
        if (this.inputCons.size() > 0);
        {
            ret = true;
        }
        return ret;
    }
    
    public boolean isConnectedFrom(Neuron n)
    {
        boolean ret = false;
        for (NConnection con : inputCons)
        {
            if(con.getFromNeuron() == n)
            {
                ret = true;
            }
        }
        return ret;
    }
    
    public boolean isConnectedTo(Neuron n)
    {
        boolean ret = false;
        for(NConnection con : outputCons)
        {
            if(con.getToNeuron() == n)
            {
                ret = true;
            }
        }
        
        return ret;
    } 
    
    public NConnection getConnectionFrom(Neuron from)
    {
        NConnection retcon = null; 
        for(NConnection con : this.inputCons)
        {
            if(con.getFromNeuron() == from)
            {
                retcon = con;
                return retcon;
            }
        }
        return retcon;
    }
    
    public void addConncetinFrom(NConnection con)
    {
        if(false == this.isConnectedFrom(con.getFromNeuron()) )
        {
            inputCons.add(con);
            Neuron fromN = con.getFromNeuron();
            fromN.addConncetinFrom(con);
        }
    }
    
    public void addConnectionFrom(Neuron n, double weight)
    {
        NConnection con = new NConnection(n, this, weight);
        this.addConncetinFrom(con);
    }
    
    public void addConectionTo(NConnection con)
    {
        if(con.getToNeuron() != this)
        {
            throw new IllegalArgumentException("Bad FromNeuron!");
        }
        
        if(false == isConnectedFrom(con.getToNeuron()))
        {
            outputCons.add(con);
        }
    }
    
    public final List<NConnection> getInputCons()
    {
        return inputCons;
    }
    
    public final List<NConnection> getOutputCons()
    {
        return outputCons;
    }
    
    private void removeInputCons(NConnection con)
    {
        inputCons.remove(con);
    }
    
    private void removeOutputCons(NConnection con)
    {
        outputCons.remove(con);
    }
    
    public void removeConnectionFrom(Neuron from)
    {
        for(NConnection con : inputCons)
        {
            if(con.getFromNeuron() == from)
            {
                from.removeInputCons(con);
                this.removeInputCons(con);
                break;
            }
        }
    }
    
    public void removeConnectionTo(Neuron to)
    {
        for(NConnection con : outputCons)
        {
            if(con.getToNeuron() == to)
            {
                to.removeOutputCons(con);
                this.removeOutputCons(con);
                break;
            }
        }
    }
    
    public void removeAllConnections()
    {
        inputCons.clear();
        outputCons.clear();
    }
    
    public String getID()
    {
        return this.id;
    }
    
    public void setID(String id)
    {
        this.id = id;
    }
    
    public void init(double initvalue)
    {
        for(NConnection con : this.inputCons)
        {
            con.setWeight(initvalue);
        }
    }
}
