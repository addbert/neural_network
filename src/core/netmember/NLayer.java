/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core.netmember;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Adrian Neubert
 */
public class NLayer
{
    private String lid;
    private List<Neuron> neurons;
    
    public NLayer(String id)
    {
        this.lid = id;
        this.neurons = new ArrayList<>();
    }
    
    public NLayer(String id, int neurons)
    {
        this.lid = id;
        this.neurons = new ArrayList<>(neurons);
    }
    
    public NLayer(String id, List<Neuron> neurons)
    {
        this.lid = id;
        this.neurons = neurons;
    }
    
    public String getID()
    {
        return this.lid;
    }
    
    public final List<Neuron> getNeurons()
    {
        return this.neurons;
    }
    
    public Neuron getNeuronByPos(int i)
    {
        return this.neurons.get(i);
    }
    
    public int getNeuronCount()
    {
        return this.neurons.size();
    }
}