/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core.netmember;

/**
 *
 * @author Adrian Neubert
 */
public class NConnection
{
    private Neuron fromNeuron, toNeuron;
    private double weight;
    
    public NConnection(Neuron from, Neuron to)
    {
        this.fromNeuron = from;
        this.toNeuron = to;
        this.weight = Math.random();
    }
    
    public NConnection(Neuron from, Neuron to, double weight)
    {
        this(from, to);
        this.weight = weight;
    }
    
    public void setWeight(double weight)
    {
        this.weight = weight;
    }
    
    public double getWeight()
    {
        return this.weight;
    }
        
    public Neuron getToNeuron()
    {
        return this.toNeuron;
    }
    
    public Neuron getFromNeuron()
    {
        return this.fromNeuron;
    }

    public double getWeightedIn()
    {
        return fromNeuron.calculateoutput() * weight;
    }
    
    @Override
    public boolean equals(Object o)
    {
        boolean ret;
        
        if (this == o)
        {
            ret = true;
        }
        else if(o == null)
        {
            ret = false;
        }
        NConnection other = (NConnection) o;
        if(fromNeuron == null)
        {
            if(other.fromNeuron != null)
            {
                ret = false;
            }
        }
        else if(!fromNeuron.equals(other.fromNeuron))
        {
            ret = false;
        }
        if(toNeuron == null)
        {
            if(other.toNeuron != null)
            {
                ret = false;
            }
        }
        else if(!toNeuron.equals(other.toNeuron))
        {
            ret = false;
        }
        if(Double.doubleToLongBits(weight) != Double.doubleToLongBits(weight))
        {
            ret = false;
        }
        else
        {
            ret = true;
        }
        return ret;
    }

    @Override
    public int hashCode()
    {
        final int p = 31;
        int hash = 1;
        hash = p * hash * ((fromNeuron == null) ? 0 : fromNeuron.hashCode());
        hash = p * hash * ((toNeuron == null) ? 0 : toNeuron.hashCode());
        long tmp;
        return hash;
    }
}
